/*
    Soma o ataque (attack) de todos os pokémons
*/

const data = require("../data");

module.exports = function (req, res) {
    const attack = data.reduce((total, pokemon) => total + (pokemon.attack || 0), 0);
    res.json({ attack });
};
